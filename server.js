"use strict";
exports.__esModule = true;
var fs = require("fs");
var path = require("path");
var express = require("express");
var LRU = require("lru-cache");
require('source-map-support').install();
var favicon = require('serve-favicon');
var compression = require('compression');
var microcache = require('route-cache');
var vue_server_renderer_1 = require("vue-server-renderer");
var resolve = function (file) { return path.resolve(__dirname, file); };
var isProd = process.env.NODE_ENV === 'prod';
var useMicroCache = process.env.MICRO_CACHE !== 'false';
var app = express();
var cache = LRU({
    max: 1000,
    maxAge: 1000 * 60 * 15
});
function createRenderer(bundle, options) {
    return vue_server_renderer_1.createBundleRenderer(bundle, Object.assign(options, {
        cache: cache,
        basedir: resolve('./dist'),
        runInNewContext: false
    }));
}
var renderer;
var readyPromise;
var templatePath = resolve('src/index.template.html');
if (isProd) {
    var template = fs.readFileSync(templatePath, 'utf-8');
    var bundle = require(resolve('./dist/vue-ssr-server-bundle.json'));
    var clientManifest = require(resolve('./dist/vue-ssr-client-manifest.json'));
    renderer = createRenderer(bundle, {
        template: template,
        clientManifest: clientManifest
    });
}
else {
    var waitParcel = require('./configs/wait-parcel')["default"];
    readyPromise = waitParcel(app, templatePath, function (bundle, options) {
        renderer = createRenderer(bundle, options);
    });
}
var serve = function (path, cache) {
    return express.static(resolve(path), {
        maxAge: cache && isProd ? 1000 * 60 * 60 * 24 * 30 : 0
    });
};
app.use(compression({ threshold: 0 }));
app.use(favicon('./public/favicon.ico'));
app.use('/dist', serve('./dist', true));
app.use('/public', serve('./public', true));
app.use('/manifest.json', serve('./manifest.json', true));
app.use('/service-worker.js', serve('./dist/service-worker.js', false));
app.use(microcache.cacheSeconds(1, function (req) { return useMicroCache && req.originalUrl; }));
var handleError = function (err, req, res) {
    if (err.url) {
        res.redirect(err.url);
    }
    else if (err.code === 404) {
        res.status(404).send(err);
    }
    else {
        if (isProd) {
            res.status(500);
        }
        else {
            var Youch = require('youch');
            var youch = new Youch(err, req);
            youch.toHTML().then(function (html) {
                res.writeHead(200, { 'content-type': 'text/html' });
                res.write(html);
                res.end();
            });
            console.error("error during render : " + req.url);
            console.error(err.stack);
        }
    }
};
function render(req, res) {
    res.setHeader('Content-Type', 'text/html');
    var context = {
        title: 'App',
        url: req.url
    };
    renderer.renderToString(context, function (err, html) {
        console.log(err)
        if (err) {
            return handleError(err, req, res);
        }
        res.send(html);
    });
}
app.get('*', isProd
    ? render
    : function (req, res) {
        readyPromise.then(function () { return render(req, res); });
    });
var port = process.env.PORT || 4000;
app.listen(port, function () {
    console.log("server started at http://localhost:" + port);
});
