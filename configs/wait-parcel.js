"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var webpack = require("webpack");
var chokidar = require("chokidar");
var client_1 = require("./webpack/client");
var server_1 = require("./webpack/server");
var MFS = require('memory-fs');
/* tslint:disable */
var readFile = function (fs, file) {
    try {
        return fs.readFileSync(path.join(client_1.default.output.path, file), 'utf-8');
    }
    catch (e) { }
};
function parcel(app, templatePath, cb) {
    var bundle;
    var template;
    var clientManifest;
    var ready;
    var reject;
    var readyPromise = new Promise(function (r, reject) {
        ready = r;
        reject = reject;
    });
    var update = function () {
        if (bundle && clientManifest) {
            ready();
            cb(bundle, {
                template: template,
                clientManifest: clientManifest
            });
        }
    };
    // read template from disk and watch
    template = fs.readFileSync(templatePath, 'utf-8');
    chokidar.watch(templatePath).on('change', function () {
        template = fs.readFileSync(templatePath, 'utf-8');
        console.log('index.html template updated.');
        update();
    });
    // modify client config to work with hot middleware
    client_1.default.entry['app'] = [
        'webpack-hot-middleware/client',
        client_1.default.entry['app']
    ];
    client_1.default.output.filename = '[name].js';
    client_1.default.plugins.push(new webpack.HotModuleReplacementPlugin(), new webpack.NoEmitOnErrorsPlugin());
    // dev middleware
    var clientCompiler = webpack(client_1.default);
    var devMiddleware = require('webpack-dev-middleware')(clientCompiler, {
        publicPath: client_1.default.output.publicPath
    });
    app.use(devMiddleware);
    clientCompiler.plugin('done', function (stats) {
        stats = stats.toJson();
        stats.errors.forEach(function (err) { return console.error(err); });
        stats.warnings.forEach(function (err) { return console.error(err); });
        if (stats.errors.length)
            return;
        clientManifest = JSON.parse(readFile(devMiddleware.fileSystem, 'vue-ssr-client-manifest.json'));
        update();
    });
    // hot middleware
    app.use(require('webpack-hot-middleware')(clientCompiler, {
        heartbeat: 5000
    }));
    // watch and update server renderer
    var serverCompiler = webpack(server_1.default);
    var mfs = new MFS();
    serverCompiler.outputFileSystem = mfs;
    serverCompiler.watch({}, function (err, stats) {
        if (err)
            throw err;
        stats = stats.toJson();
        if (stats.errors.length)
            return;
        bundle = JSON.parse(readFile(mfs, 'vue-ssr-server-bundle.json'));
        update();
    });
    return readyPromise;
}
exports.default = parcel;
//# sourceMappingURL=wait-parcel.js.map