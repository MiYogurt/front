"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("./utils");
var webpack_1 = require("webpack");
var isProd = 'prod' === process.env.NODE_ENV;
var DllConfiguration = {
    mode: isProd ? 'production' : 'development',
    output: {
        path: utils_1.getPath('dist'),
        filename: '[name].js',
        library: '[name]'
    },
    entry: {
        libs: [
            'vue',
            'vue-router',
            'vue-rx',
            'vuex',
            'reflect-metadata',
            'es6-promise'
        ]
    },
    plugins: [
        new webpack_1.DllPlugin({
            context: utils_1.getPath(''),
            path: 'manifest.json',
            name: '[name]'
        })
    ]
};
exports.default = DllConfiguration;
//# sourceMappingURL=dll.js.map