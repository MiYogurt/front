"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var merge = require("webpack-merge");
var VueSSRClientPlugin = require("vue-server-renderer/client-plugin");
var base_1 = require("./base");
var webpack_bundle_analyzer_1 = require("webpack-bundle-analyzer");
var webpack_1 = require("webpack");
var utils_1 = require("./utils");
var isProd = 'prod' === process.env.NODE_ENV;
var clientConfiguration = merge(base_1.default, {
    entry: {
        app: utils_1.getPath('src/entry-client.ts')
    },
    resolve: {
        extensions: [
            '.ts',
            '.tsx',
            '.js',
            '.vue',
            '.json',
            '.styl',
            '.css',
            '.sass',
            '.scss'
        ],
        alias: {
            vue$: 'vue/dist/vue.esm.js',
            api: utils_1.getPath('src/api/v1/client/index.ts')
        }
    },
    externals: ['ramda', 'rxjs'],
    plugins: [
        // new DllReferencePlugin({
        //   context: getPath(''),
        //   manifest: 'manifest.json'
        // }),
        new webpack_1.DefinePlugin({
            isClient: true,
            isServer: false,
            Rx: 'window.Rx',
            R: 'window.R'
        }),
        new VueSSRClientPlugin()
    ]
});
if (isProd) {
    clientConfiguration.plugins.push(new webpack_bundle_analyzer_1.BundleAnalyzerPlugin());
    clientConfiguration.optimization = {
        splitChunks: {
            name: 'common',
            chunks: 'initial',
            minSize: 0
        },
        minimize: true
    };
}
exports.default = clientConfiguration;
//# sourceMappingURL=client.js.map