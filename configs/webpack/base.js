"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
var ramda_1 = require("ramda");
var utils_1 = require("./utils");
var isProd = 'prod' === process.env.NODE_ENV;
var commonPlugins = [
    new webpack.DefinePlugin({
        API_URL: 'http://miao.nodelover.me/api/v1'
    })
    // new ProvidePlugin({
    //   R: ['ramda/dist/ramda.js'],
    //   Rx: ['rxjs']
    // })
];
var concatCommon = ramda_1.concat(commonPlugins);
var prodPlugins = concatCommon([
    new ExtractTextPlugin({
        filename: 'common.[hash].css'
    })
]);
// const devPlugins = concatCommon([new FriendlyErrorsWebpackPlugin()])
var devPlugins = concatCommon([]);
var babelrc = {
    presets: [
        [
            'vue-app',
            {
                useBuiltIns: true
            }
        ],
        'env'
    ],
    plugins: [
        [
            'ramda',
            {
                useES: true
            }
        ]
    ]
};
var publicPath = '/dist/';
var BaseConfiguration = {
    devtool: isProd ? false : 'inline-source-map',
    mode: isProd ? 'production' : 'development',
    output: {
        path: utils_1.getPath('dist'),
        chunkFilename: '[name].[hash].bundle.js',
        filename: '[name].[hash].js',
        publicPath: '/dist/'
    },
    resolve: {
        alias: {
            public: utils_1.getPath('public'),
            vue$: 'vue/dist/vue.esm.js'
        },
        extensions: ['.ts', '.tsx', '.js', '.vue', '.json'],
        plugins: [
            new TsconfigPathsPlugin({
                configFile: utils_1.getPath('.'),
                logLevel: 'info',
                extensions: ['.ts', '.tsx', 'vue', 'sass']
            })
        ]
    },
    plugins: isProd ? prodPlugins : devPlugins,
    performance: {
        maxEntrypointSize: 300000,
        hints: isProd ? 'warning' : false
    },
    module: {
        unknownContextCritical: false,
        // noParse: /es6-promise/,
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    extractCSS: isProd,
                    loaders: {
                        scss: 'vue-style-loader!css-loader!sass-loader',
                        // sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax!sass-resources-loader?resources=' + getPath('src/_vars.sass'),
                        sass: [
                            'vue-style-loader',
                            'css-loader',
                            {
                                loader: 'sass-loader',
                                options: {
                                    indentedSyntax: true
                                }
                            },
                            {
                                loader: 'sass-resources-loader',
                                options: {
                                    resources: [
                                        utils_1.getPath('src/sass/_vars.sass'),
                                        utils_1.getPath('src/sass/_func.sass')
                                    ]
                                }
                            }
                        ],
                        styl: 'vue-style-loader!css-loader!stylus-loader'
                    }
                }
            },
            { test: /\.styl$/, loader: 'vue-style-loader!css-loader!stylus-loader' },
            { test: /\.css$/, use: ['style-loader', 'css-loader'] },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [{ loader: 'babel-loader', options: babelrc }]
            },
            {
                test: /\.sass$/,
                loader: 'vue-style-loader!css-loader!sass-loader!sass-resources-loader?resources=' +
                    utils_1.getPath('src/sass/_vars.sass') +
                    '&resources=' +
                    utils_1.getPath('src/sass/_func.sass')
            },
            { test: /\.pug$/, loader: 'pug-loader', options: { doctype: 'html' } },
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: { appendTsSuffixTo: [/\.vue$/] }
            },
            {
                test: /\.tsx$/,
                exclude: /node_modules/,
                use: [{ loader: 'babel-loader', options: babelrc }, 'ts-loader']
            },
            {
                test: /\.(png|jpg|gif|svg|ttf|woff|eot)$/,
                loader: 'file-loader',
                options: { name: '[name].[ext]?[hash]' }
            }
        ]
    }
};
exports.default = BaseConfiguration;
//# sourceMappingURL=base.js.map