"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ramda_1 = require("ramda");
var path_1 = require("path");
var getPath = ramda_1.curryN(3, path_1.resolve)(__dirname, '../..');
exports.getPath = getPath;
//# sourceMappingURL=utils.js.map