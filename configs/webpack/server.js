"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var webpack = require("webpack");
var merge = require("webpack-merge");
var nodeExternals = require("webpack-node-externals");
var VueSSRServerPlugin = require("vue-server-renderer/server-plugin");
var base_1 = require("./base");
var utils_1 = require("./utils");
exports.default = merge(base_1.default, {
    target: 'node',
    devtool: 'source-map',
    entry: utils_1.getPath('src/entry-server.ts'),
    output: {
        filename: 'server-bundle.js',
        libraryTarget: 'commonjs2'
    },
    resolve: {
        alias: {
            api: utils_1.getPath('src/api/v1/server/index.ts')
        },
        extensions: [
            '.ts',
            '.tsx',
            '.js',
            '.vue',
            '.json',
            '.styl',
            '.css',
            '.sass',
            '.scss'
        ]
    },
    externals: [
        nodeExternals({
            whitelist: [/\.css$/, /\.styl$/, /\.vue$/, /vue-quill-editor/]
        }),
        'quill'
    ],
    plugins: [
        new webpack.DefinePlugin({
            isServer: true,
            isClient: false
        }),
        new webpack.ProvidePlugin({
            R: ['ramda/dist/ramda.js'],
            Rx: ['rxjs']
        }),
        new VueSSRServerPlugin()
    ]
});
//# sourceMappingURL=server.js.map