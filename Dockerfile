FROM node:9.2.0

RUN ["mkdir", "-p", "/usr/src/application"]

WORKDIR /usr/src/application

COPY package.json /usr/src/application/

RUN ["npm", "install", "--production" , "--registry=https://registry.npm.taobao.org"]

COPY . /usr/src/application

EXPOSE 4000

CMD NODE_ENV=prod node server.js
