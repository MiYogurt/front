import Vue from 'vue';
function isObject(obj) {
    const type = typeof obj;
    return obj != null && (type == 'object' || type == 'function');
}
function prop(key) {
    return (obj) => {
        return obj[key];
    };
}
function pathOr(p, placeholder) {
    return (obj) => {
        let c = obj;
        for (const i of p) {
            if (!isObject(c)) {
                return placeholder;
            }
            else if (c !== null) {
                c = c[i];
            }
        }
        return c;
    };
}
function Const(x) {
    return {
        value: x,
        map: (f) => Const(x)
    };
}
function Identity(x) {
    return {
        value: x,
        map: (f) => Identity(f(x))
    };
}
// mutable assoc
function assoc(k, v, o) {
    Vue.set(o, k.toString(), v);
    return o;
}
// mutable assocPath
function assocPath(path, v, o) {
    let obj = o;
    for (let i = 0; i < path.length - 1; i++) {
        if (obj[path[i]] === undefined) {
            Vue.set(obj, path[i], {});
        }
        obj = obj[path[i]];
    }
    obj[path[path.length - 1]] = v;
    return o;
}
function lens(getter, setter) {
    return (toFunctorFn) => {
        return (obj) => {
            return toFunctorFn(getter(obj)).map((v) => {
                return setter(v, obj);
            });
        };
    };
}
function set(len, value, obj) {
    const functor = len((x) => Identity(value))(obj);
    return obj;
}
function get(len, obj) {
    const functor = len((x) => Const(x))(obj);
    return functor.value;
}
function over(len, fn, obj) {
    const functor = len((x) => Identity(fn(x)))(obj);
    return functor.value;
}
function propLens(name) {
    return lens(prop(name), (v, obj) => assoc(name, v, obj));
}
function pathLensOr(path, alt) {
    return lens(pathOr(path, alt), (v, obj) => assocPath(path, v, obj));
}
function lensToVueLens(len) {
    return (obj) => {
        return Object.defineProperty({}, 'value', {
            get: () => get(len, obj),
            set: v => set(len, v, obj)
        });
    };
}
export { Const, Identity, lensToVueLens, pathLensOr, propLens, over, get, set, lens };
//# sourceMappingURL=vuex-lens.js.map