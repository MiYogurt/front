export class State {
    constructor() {
        this.ImageComments = [];
        this.User = {};
        this.description = '';
        this.id = 0;
        this.list_url = '';
        this.main_url = '';
        this.user_id = 0;
        this.view_id = 0;
        this.updated_at = '';
        this.created_at = '';
    }
}
const imageFolder = 'http://localhost:7001/public/download/';
const concatImageFolder = imageName => imageFolder + imageName + ',1920,1080.jpg';
const Image = {
    namespaced: true,
    state: new State(),
    getters: {
        main(state) {
            return concatImageFolder(state.main_url);
        },
        list(state) {
            if (!state.list_url.length) {
                return [];
            }
            return state.list_url.split(',').map(concatImageFolder);
        }
    },
    mutations: {
        SET_IMAGE(state, payload) {
            Object.assign(state, payload);
        },
        PUSH_COMMENT(state, payload) {
            state.ImageComments.push(payload);
        }
    },
    actions: {}
};
export default Image;
//# sourceMappingURL=index.js.map