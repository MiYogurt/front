import { propLens, over, set } from '../../../shims/vuex-lens';
const imagesLen = propLens('images');
export class State {
    constructor() {
        this.images = [];
        this.all = 1;
        this.page = 1;
    }
}
const Index = {
    namespaced: true,
    state: new State(),
    mutations: {
        PUSH_IMAGES: (state, payload) => {
            over(imagesLen, (images) => R.concat(images, payload), state);
        },
        SET_IMAGES: (state, payload) => {
            set(imagesLen, payload, state);
        },
        ALL_PAGE: (state, payload) => {
            state.all = parseInt(payload);
        },
        PAGE: (state, payload) => {
            state.page = parseInt(payload);
        }
    },
    actions: {
        setImages({ commit }, images) {
            commit('SET_IMAGES', images.data);
            commit('ALL_PAGE', images.allPage);
            commit('PAGE', images.currentPage);
        },
        pushImages({ commit }, images) {
            commit('PUSH_IMAGES', images.data);
            commit('ALL_PAGE', images.allPage);
            commit('PAGE', images.currentPage);
        }
    }
};
export default Index;
//# sourceMappingURL=index.js.map