import Vue from 'vue';
import Vuex from 'vuex';
import index from './modules/index';
import image from './modules/image';
Vue.use(Vuex);
export class State {
    constructor() {
        this.user_token = null;
        this.user_info = null;
    }
}
const options = {
    state: new State(),
    mutations: {
        USER_TOKEN(state, payload) {
            state.user_token = payload;
        },
        USER_INFO(state, payload) {
            state.user_info = payload;
        }
    },
    getters: {},
    actions: {},
    modules: {
        index,
        image
    }
};
export default function createStore() {
    return new Vuex.Store(options);
}
//# sourceMappingURL=index.js.map